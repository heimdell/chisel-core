
module Range where

import System.FilePath hiding ((<.>))

import Pretty

-- | A range in source code.
--
data Range = Range
  { from :: Point
  , to   :: Point
  , file :: FilePath
  }

-- | A point in source code.
--
data Point = Point
  { line :: Int
  , col  :: Int
  }

instance Pretty Point where
  pp (Point l c) = pp l <.> ":" <.> pp c

instance Pretty Range where
  pp (Range f t n) = pp n <.> "," <+> pp f <.> "-" <.> pp t
