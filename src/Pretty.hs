
module Pretty (module Pretty, module Text.PrettyPrint) where

import Data.Text (Text, unpack)

import Text.PrettyPrint hiding ((<>), Str)

-- | Ability to be pretty-printed.
--
class Pretty p where
  pp :: p -> Doc

-- | Ability to be pretty-printed, but for functors.
--
class Pretty1 p where
  pp1 :: p Doc -> Doc

instance {-# overlappable #-} (Pretty a, Pretty1 p, Functor p) => Pretty (p a) where
  pp = pp1 . fmap pp

instance Pretty String where pp = text

instance Pretty Doc    where pp = id
instance Pretty Int    where pp = int
instance Pretty Double where pp = double
instance Pretty Text   where pp = pp . unpack
instance Pretty ()     where pp = const "()"

instance (Pretty a, Pretty b) => Pretty (a, b) where
  pp (a, b) = "(" <.> pp a <.> "," <+> pp b <.> ")"

instance Pretty1 [] where
  pp1 = brackets . fsep . punctuate ","

instance {-# overlappable #-} Pretty x => Show x where
  show = show . pp

infixl 6 <.>

-- | Replacement for `<>`, whcich /can/ be used with `<+>` in one expression.
--
(<.>) :: Doc -> Doc -> Doc
(<.>) = (<>)

-- | Make an s-expression.
--
sexpr :: Doc -> [Doc] -> Doc
sexpr header items = "(" <.> header `indent` foldr above empty items <.> ")"

-- | Indent second item off the first.
--
indent :: Doc -> Doc -> Doc
indent = flip hang 2

-- | Put second item right below the first.
--
above :: Doc -> Doc -> Doc
above = flip hang 0

-- | Wrapper to generate `Show` instance from `Pretty` instance.
--
newtype PP a = PP { unPP :: a }

instance Pretty a => Show (PP a) where
  show = show . pp . unPP
