{- |
  DSL for parsing stuff.
-}
module Parsing (module Parsing, module Control.Applicative) where

import Control.Applicative (Alternative (..), some, many)
import Control.Comonad.Cofree (Cofree (..))
import Control.Monad (ap, liftM, void)

import Data.Char (isSpace)
import Data.Monoid (Sum (..))
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.IO as Text

import Range

-- | Diff-parser based on "Update monad".
--
--   @d@ is an offset type, in terms of stream @s@.
--
newtype Parser e d s a = Parser
  { unParser :: s -> (d, Either [e] a)
  }

-- | Apply offset @d@ to stream @s@.
--
class (Monoid d, Ord d) => Action d s where
  act :: d -> s -> s

instance Action d s => Monad (Parser e d s) where
  Parser p >>= callb = Parser \s ->
    let (d, ea) = p s
    in case ea of
      Left e -> (d, Left e)
      Right a ->
        let (d', b) = callb a `unParser` act d s
        in (d <> d', b)

instance Action d s => Applicative (Parser e d s) where
  pure x = Parser \_ -> (mempty, Right x)
  (<*>)  = ap

instance Action d s => Functor (Parser e d s) where
  fmap = liftM

-- | The parser is LL(1) in general. Use `try` to backtrack.
--
instance Action d s => Alternative (Parser e d s) where
  empty = Parser \_ -> (mempty, Left empty)

  Parser l <|> Parser r = Parser \s ->
    let (d, ea) = l s
    in case ea of
      Left e | d == mempty ->
        let (d', ea') = r s
        in case ea' of
          Left e' | d' == mempty -> (d, Left (e <> e'))
          _ -> (d', ea')
      _ -> (d, ea)

instance Action d s => MonadFail (Parser e d s) where
  fail = error

-- | Left-biased choice.
--
select :: Action d s => [Parser e d s a] -> Parser e d s a
select = foldl1 (<|>)

-- | If parser fails, ignore if it consumed anything.
--
try :: Monoid d => Parser e d s a -> Parser e d s a
try (Parser p) = Parser \s ->
  case p s of
    (_, Left e) -> (mempty, Left e)
    other       -> other

-- | Terminate with given error.
--
die :: Monoid d => e -> Parser e d s a
die msg = Parser \_ -> (mempty, Left [msg])

-- | Replace the error for the parser.
--
(<?>) :: Parser e d s a -> e -> Parser e d s a
Parser p <?> msg = Parser \s ->
  case p s of
    (d, Left _) -> (d, Left [msg])
    other       -> other

-- | Token stream.
--
data Stream el = Stream
  { point  :: Point
  , fp     :: FilePath
  , stream :: [(Range, el)]
  }
  deriving Show

instance Action (Sum Int) (Stream el) where
  act (Sum n) = foldl (.) id $ replicate n step'

step' it = maybe it fst $ step it

-- | Try peeking a token.
--
step itself@(Stream pt fp els) = case els of
  []                          -> Nothing
  [(r, el)]                   -> Just (Stream (to r) fp [], el)
  (_, el) : rest@((r, _) : _) -> Just (Stream (from r) fp rest, el)

-- | Check if `Stream` is finished.
--
isEos (Stream _ _ s) = null s

-- | Concrete parser.
--
type Parser' e el = Parser e (Sum Int) (Stream el)

-- | Returns a result of matching the next token.
--
match :: (el -> Maybe a) -> Parser' e el a
match recog = Parser \s ->
  case step s of
    Just (_, recog -> Just a) -> (1, Right a)
    _                         -> (0, Left [])

-- | Assert that this is the next token.
--
token :: Eq el => el -> Parser' el el ()
token el = void (match $ does $ (el ==)) <?> el

-- | Get current filepath.
--
filePath :: Parser' e el FilePath
filePath = Parser \s -> (0, Right $ fp s)

-- | Attach a range to the parser's result.
--
ranged :: Functor f => Parser' e el (f (Cofree f Range)) -> Parser' e el (Cofree f Range)
ranged fa = do
  fp   <- filePath
  from <- position
  f    <- fa
  to   <- position
  return $ Range from to fp :< f

-- | Plumbing, turns predicate into a matcher.
--
does :: (a -> Bool) -> (a -> Maybe a)
does p a
  | p a = Just a
  | otherwise = Nothing

-- | Get current position.
--
position :: Parser' e el Point
position = Parser \s -> (0, Right $ point s)

-- | Split the `Text` into a `Stream` of tokens.
--
tokenize :: FilePath -> Text -> Stream Text
tokenize fp = Stream (Point 1 1) fp . go (Point 1 1)
  where
    go pt txt = case Text.uncons txt of
      Just (c, rest) -> do
        let pt' = advance c pt
        if
          | isSpace c ->
            go pt' rest
          | c == '<' -> do
            let (token, Text.drop 1 -> rest') = Text.breakOn ">" txt
            let pt'' = advance '>' $ Text.foldl (flip advance) pt' token
            (Range pt pt'' fp, token <> ">") : go pt'' rest'
          | c `elem` ['(', ')', '/', '%'] ->
            (Range pt pt' fp, Text.singleton c) : go pt' rest
          | c == ';' -> do
            let (comment, rest) = Text.breakOn "\n" txt
            let pt'' = Text.foldl (flip advance) pt comment
            go pt'' rest
          | otherwise -> do
            let (token, rest') = Text.break (\c -> isSpace c || c `elem` ['<', '(', ')', '/', '%', ';']) txt
            let pt'' = Text.foldl (flip advance) pt' token
            (Range pt pt'' fp, token) : go pt'' rest'
      _ -> []

-- | Advance the position by one char.
--
advance :: Char -> Point -> Point
advance '\n' (Point l _) = Point (l + 1) 1
advance  _   (Point l c) = Point l (c + 1)

-- | Ability to be parsed.
--
class CanBeParsed x where
  parser :: Parser' Text Text x

-- | Parse something.
--
parse :: forall x. CanBeParsed x => FilePath -> IO (Either (Point, [Text]) x)
parse fp = do
  txt <- Text.readFile fp
  let s = tokenize fp txt
  case unParser parser s of
    (d, Left errs) -> return $ Left (point $ act d s, errs)
    (_, Right a)   -> return $ Right a

-- | Parse something, or die trying.
--
parse' :: forall x. CanBeParsed x => FilePath -> IO x
parse' fp = do
  txt <- Text.readFile fp
  let s = tokenize fp txt
  case unParser parser s of
    (d, Left errs) -> error (show (point $ act d s, errs))
    (_, Right a)   -> return a
