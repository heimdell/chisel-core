
{-|

  This is a toy virtual machine for a simple call-by-need (`Updatable`) language.

  The program is encoded as a value of type `Program` and the evaluation result
  is of type `Value`.

-}

module Runtime where

import Control.Comonad.Cofree (Cofree (..))
import Control.Monad.State (StateT, runStateT, lift)

import Data.Foldable (for_)
import Data.Traversable (for)
import qualified Data.Map as Map

import Memory
import Program
import Pretty
import Struct

import Debug.Trace

-- | Evaluation result.
--
data Value i where
  Clos :: { closure :: (Struct Addr, Function i) } -> Value i  -- ^ Function with context.
  VC   :: { con     :: Constant }                  -> Value i  -- ^ Constant.
  Seq  :: { konts   :: [Kont i] }                  -> Value i  -- ^ Delimited continuation
  Adt  :: { ctor    :: CtorName, args :: [Addr] }  -> Value i  -- ^ ADT.
  deriving Show via PP (Value i)
  deriving stock Functor

-- | Representation for all function-like entities.
--
data Function i where
  Closure   :: { env :: Context, args :: [Name], body :: Program i } -> Function i  -- ^ Normal function.
  Builtin   :: BifName                                               -> Function i  -- ^ Built-in.
  Construct :: CtorName                                              -> Function i  -- ^ Constructor.
  deriving Show via PP (Function i)
  deriving stock Functor

-- | What to do next with current value.
--
data Kont i where
  Call        :: { arg  :: [Memory.Addr] }             -> Kont i  -- ^ current_value(arg)
  ApplyStrict :: { fun  :: (Struct Addr, Function i) } -> Kont i  -- ^ fun(.., current_value, ..)
  Rewrite     :: { lazy :: Memory.Addr }               -> Kont i  -- ^ Rewrite the node with current value.
  Prompt      :: { mark :: Int }                       -> Kont i  -- ^ Continuation delimiter.
  Select      :: { iD   :: CtorName
                 , yes  :: ([Name], (Context, Program i))
                 , no   :: Memory.Addr }               -> Kont i  -- ^ If-construct.
  deriving Show via PP (Kont i)
  deriving stock Functor

-- | A mutable cell with either value or program/context pair.
--
data Cell i where
  OneShot   :: { env :: Context, program :: Program i }                 -> Cell i  -- ^ Non-updatable cell.
  Updatable :: { env :: Context, program :: Program i, itself :: Addr } -> Cell i  -- ^ Updatable cell.
  Ready     :: { result :: Value i }                                    -> Cell i  -- ^ Result.
  deriving Show via PP (Cell i)
  deriving stock Functor

-- | Program environment.
--
type Context = [(Name, Memory.Addr)]

-- | A VM state to work in.
--
type CESK i =
  ( Cell i
  , Memory.T (Cell i)
  , [Kont i]
  , Bool
  )

-- | Readonly environment (built-in functions).
--
type Env i = Map.Map BifName (BIF i)

-- | Wrapper for built-in function.
newtype BIF i = BIF { runBIF :: [Value i] -> IO (Value i) }

-- | Small-step semantics (evaluate one step).
--
{-# inline step #-}
step
  :: Pretty i
  => Env i        -- ^ Builtin functions.
  -> CESK i       -- ^ VM State.
  -> IO (CESK i)
step bifs state@(c, _, k, _) = do
--  print (c, k)
 case state of
  (v@Ready {}, mem, Rewrite addr : k, go) -> do
    let mem' = Memory.put addr v mem
    return (v, mem', k, go)

  (cell, mem, Call [] : k, go) -> do
    return (cell, mem, k, go)

  (Ready (Clos (clo, f)), mem, k, go) | saturated clo -> do
    let args = grabArgs clo
    case f of
      Closure env names body -> do
        return (OneShot (zip names args ++ env) body, mem, k, go)

      Builtin bifName -> do
        let
          values = flip map args \arg -> do
            let cell = Memory.get arg mem
            case cell of
              Ready v -> v
              _       -> error "NonStrict argument to builtin!"
        let BIF func = bifs Map.! bifName
        v' <- func values
        return (Ready v', mem, k, go)

      Construct ctor -> do
        return (Ready (Adt ctor args), mem, k, go)

  (Ready v, mem, ApplyStrict (clo, f) : k, go) -> do
    let (mem', x) = Memory.new (Ready v) mem
    return (Ready (Clos (push x clo, f)), mem', k, go)

  (Ready (Clos (clo, f)), mem, Call (x : xs) : k, go) -> do
    let mem' = Memory.change x (makeCellUpdatable x) mem
    if isNextArgStrict clo
    then do
      let cell' = Memory.get x mem'
      return (cell', mem', ApplyStrict (clo, f) : Call xs : k, go)
    else do
      return (Ready (Clos (push x clo, f)), mem', Call xs : k, go)

  (Ready (Seq dk), mem, Call (x : xs) : k, go) -> do
    let cell' = Memory.get x mem
    return (cell', mem, dk ++ Call xs : k, go)

  (Ready (Adt ctor xs), mem, Select ctor' (names, (env, yes)) no : k, go) -> do
    let
      p' = if ctor == ctor'
        then OneShot (zip names xs ++ env) yes
        else Memory.get no mem
    return (p', mem, k, go)

  (OneShot env (_ :< Var name), mem, k, go) -> do
    let v' = Memory.get (findName name env) mem
    return (v', mem, k, go)

  (OneShot env (_ :< App f xs), mem, k, go) -> do
    let (mem', xs') = Memory.massNew (map (OneShot env) xs) mem
    return (OneShot env f, mem', Call xs' : k, go)

  (OneShot env (_ :< Lam args prog), mem, k, go) -> do
    let v' = Clos (construct (nonStrict (length args)), Closure env args prog)
    return (Ready v', mem, k, go)

  (OneShot _ (_ :< PC (Bif arity bif)), mem, k, go) -> do
    let v' = Clos (construct (strict arity), Builtin bif)
    return (Ready v', mem, k, go)

  (OneShot _ (_ :< PC constant), mem, k, go) -> do
    return (Ready (VC constant), mem, k, go)

  (OneShot env (_ :< Let decls body), mem, k, go) -> do
    let (mem', env') = pushDecls env decls mem
    return (OneShot env' body, mem', k, go)

  (OneShot env (_ :< Rst prompt body), mem, k, go) -> do
    return (OneShot env body, mem, Prompt prompt : k, go)

  (OneShot env (_ :< Sht prompt exit consumer), mem, k, go) -> do
    let (func, Prompt _ : k') = break (isPrompt prompt) k
    let (mem', addr') = Memory.new (Ready (Seq func)) mem
    let env' = (exit, addr') : env
    return (OneShot env' consumer, mem', k', go)

  (OneShot _ (_ :< Ctr ctor arity), mem, k, go) -> do
    let p' = Clos (construct (nonStrict arity), Construct ctor)
    return (Ready p', mem, k, go)

  (OneShot env (_ :< If ctor subj (names, yes) no), mem, k, go) -> do
    let (mem', no') = Memory.new (OneShot env no) mem
    return (OneShot env subj, mem', Select ctor (names, (env, yes)) no' : k, go)

  (Updatable env program src, mem, k, go) -> do
    return (OneShot env program, mem, Rewrite src : k, go)

  (v, mem, Prompt _ : k, go) -> do
    return (v, mem, k, go)

  (cell, mem, k, _) -> do
    return (cell, mem, k, False)

-- | Check if continuation is a given prompt.
isPrompt :: Int -> Kont i -> Bool
isPrompt i (Prompt i') = i == i'
isPrompt _  _          = False

-- | Allocate nodes for let-declarations.
--
--   Implemented via continuation-passing for speed and to prevent
--   <<loop>> exception.
--
pushDecls
  :: forall i
  .  Pretty i
  => Context
  -> [(Name, Program i)]
  -> Memory.T (Cell i)
  -> (Memory.T (Cell i), Context)
pushDecls e ds mem = (mem', e')
  where
    (mem', e') = go mem ds (,)

    go mem [] return = return mem e
    go mem ((n, p) : ds') return = do
      let (mem', c) = Memory.new (Updatable e' p c) mem
      go mem' ds' \mem'' de -> do
        return mem'' ((n, c) : de)

-- | Rewrite given cell into `Updatable` one.
--
makeCellUpdatable :: Addr -> Cell i -> Cell i
makeCellUpdatable s c = case c of
  OneShot e p -> Updatable e p s
  _           -> c

-- | Find a name in the environment.
--
findName n e =
  case lookup n e of
    Just it -> it
    Nothing -> error ("undefined: " ++ show n)

-- | Evaluation loop. Runs `step` up to given gas limit (1 per `step`).
--
stepNoMoreThan :: Pretty i => Int -> Env i -> CESK i -> IO (CESK i)
stepNoMoreThan n env state = go n state
  where
    go 0 state = return state
    go n state = do
      state' <- step env state
      go (n - 1) state'

-- | Evaluate the program.
--
--   Returns a `Cell`, not a `Value, because reasons.
--
eval :: Pretty i => Program i -> IO (Cell i)
eval prog = do
  let
    plus = BIF \[VC (Num x), VC (Num y)] -> return $ VC $ Num (x + y)
    less = BIF \[VC (Num x), VC (Num y)] -> return $ Adt (if x < y then CtorName "True" else CtorName "False") []
  let
    bifs =
      Map.fromList
        [ (BifName "+", plus)
        , (BifName "less", less)
        ]
  let start = (OneShot [] prog, Memory.empty, [], True)
  (a, _, _, _) <- stepNoMoreThan 100000000 bifs start
  return a

instance Pretty1 Value where
  pp1 = \case
    Clos (s, clo) -> "(" <.> pp clo <+> pp s <.> ")"
    VC  c         -> pp c
    Seq _         -> "<k>"
    Adt c ns      -> sexpr (pp c) (map pp ns)

instance Pretty1 Function where
  pp1 = \case
    Closure   env names body -> sexpr (fsep (map pp names) <+> "->") [pp body]
    Builtin   bifName        -> pp bifName
    Construct ctor           -> pp ctor

instance Pretty1 Cell where
  pp1 = \case
    OneShot   e program   -> "(s" <+> pp program <+> pp e <.> ")"
    Updatable e program _ -> "(u" <+> pp program <+> pp e <.> ")"
    Ready       value     -> "." <+> pp value

instance Pretty1 Kont where
  pp1 = \case
    Call        xs               -> sexpr "call" (map pp xs)
    ApplyStrict (clo, bif)       -> sexpr (pp bif) [pp clo]
    Rewrite     lazy             -> sexpr "rewrite" [pp lazy]
    Prompt      p                -> sexpr "prompt" [pp p]
    Select      c (ns, (_, y)) n -> sexpr "if" [pp c, sexpr "then" (map pp ns ++ ["->", pp y]), pp n]
