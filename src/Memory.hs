
{-# language DataKinds #-}
{-# language GADTs #-}

{-|
  Implementation for memory.
-}

module Memory
  ( -- * Interface
    T
  , Addr
  , empty
  , new
  , massNew
  , get
  , put
  , change
  ) where

import qualified Data.IntMap as Map

-- import Tuple

-- | An abstract datatype for memory. Is based on @Map.IntMap@.
--
data T x = Memory
  { pointer :: Int
  , grid    :: Map.IntMap x
  }

-- | Memory address.
--
type Addr = Int

-- | Create an empty memory block.
--
empty :: T x
empty = Memory 0 Map.empty

-- | Allocate an item in the memory, return allocation address.
--
new :: x -> T x -> (T x, Addr)
new x (Memory ptr m ) =
  (Memory (ptr + 1) (Map.insert ptr x m), ptr)

massNew :: [x] -> T x -> (T x, [Addr])
massNew list mem = massNewCont list mem (,)
  where
    massNewCont :: [x] -> T x -> (T x -> [Addr] -> c) -> c
    massNewCont [] mem ret = ret mem []
    massNewCont (x : xs) mem ret = do
      let (mem', addr) = new x mem
      massNewCont xs mem' \mem'' addrs ->
        ret mem'' (addr : addrs)

-- | Read memory at address.
--
get :: Addr -> T x -> x
get ptr (Memory _ m) = m Map.! ptr

-- | Write memory at address.
put :: Addr -> x -> T x -> T x
put c x (Memory ptr m) = Memory ptr $ Map.insert c x m

change :: Addr -> (x -> x) -> T x -> T x
change addr f (Memory ptr grid) = Memory ptr (Map.adjust f addr grid)
