
module Struct where

import Pretty

-- | Description for parameters.
--
--   If element is `True`, the parameter must be evaluated before passing to
--    the function (aka "strict").
--
data Plan where
  Plan :: { arity' :: [Bool] } -> Plan

-- | Parameter collector.
--
data Struct c where
  Struct :: { arity :: [Bool], args :: [c] } -> Struct c
  deriving Show via PP (Struct c)
  deriving stock Functor

-- | Make a collector from description.
--
construct :: Plan -> Struct c
construct (Plan arity) = Struct arity []

-- | Check if next arg is strict.
--
isNextArgStrict :: Struct c -> Bool
isNextArgStrict (Struct (a : _) _) = a

-- | Check if no more arg is needed.
--
saturated :: Struct c -> Bool
saturated (Struct arity _) = null arity

-- | Add an arg.
--
push :: c -> Struct c -> Struct c
push v (Struct (a : rity) args) = Struct rity (v : args)

-- | Get all arguments.
--
grabArgs :: Struct c -> [c]
grabArgs (Struct _ args) = reverse args

instance (Pretty c) => Pretty (Struct c) where
  pp (Struct c args) = sexpr "#"
    (  map pp args
    ++ map (\strict -> if strict then "!_" else "_") c
    )

-- | Require @n@ strict arguments.
--
strict :: Int -> Plan
strict ari = Plan (replicate ari True)

-- | Require @n@ non-strict arguments.
--
nonStrict :: Int -> Plan
nonStrict ari = Plan (replicate ari False)

-- | Grab strictness from the list.
--
exactStrictness :: [(a, Bool)] -> Plan
exactStrictness = Plan . map snd
