
module Constant where

import Data.Text (Text)
import qualified Data.Text as Text

import Pretty
import Parsing

-- | Constant.
--
data Constant where
  Str :: { str :: Text }                  -> Constant  -- ^ <hello, world>
  Num :: { num :: Double }                -> Constant  -- ^ 1
  Bif :: { arity :: Int, bif :: BifName } -> Constant  -- ^ (x, y) => x + y
  deriving Show via PP Constant

-- | Wrapper for built-in function name.
newtype BifName = BifName { unBifName  :: Text }
  deriving stock (Eq, Ord)

instance Pretty BifName where
  pp (BifName n) = pp n

instance Pretty Constant where
  pp = \case
    Str s   -> doubleQuotes $ pp s
    Num d   -> pp d
    Bif a n -> "#" <.> pp n <.> "/" <.> pp a

-- | Parser for constant.
instance CanBeParsed Constant where
  parser = select
    [ string  <?> "string literal"
    , number  <?> "number"
    , builtin <?> "built-in function"]
    where
      string = do
        s <- match $ does $ Text.isPrefixOf "<"
        return $ Str $ Text.tail $ Text.init s

      builtin = try $ do
        s <- match $ does $ ('#' ==) . Text.head
        match $ does $ ("/" ==)
        Num n <- number
        return $ Bif (round n) $ BifName (Text.tail s)

-- | Parser for number literals.
number = try $ do
  s <- match $ does $ (`elem` ['0'..'9'] ++ ['-']) . Text.head
  let reading = reads $ Text.unpack s
  case reading of
    [(it, "")] -> return $ Num it
    _          -> die "number"

