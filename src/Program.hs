
module Program (module Program, module Constant, module Name) where

import Control.Comonad.Cofree

import Data.List (intercalate)
import Data.Text (Text)
import qualified Data.Text as Text

import Constant
import Name
import Pretty
import Parsing
import Range

-- | A program tree.
--
type Program = Cofree Program_

-- | Source `Program_`.
--
data Program_ self where
  Var :: { name :: Name } -> Program_ self         -- ^ x

  App :: { fun       ::  self
         , arguments :: [self] } -> Program_ self  -- ^ f(x)

  Lam :: { args :: [Name]
         , body :: self } -> Program_ self         -- ^ (n) => prog

  Let :: { decls :: [(Name, self)]
         , body  :: self } -> Program_ self        -- ^ let f = (x) => 1 + x; f (f x)

  PC  :: { con :: Constant } -> Program_ self      -- ^ 1 OR (x, y) => x + y

  Rst :: { mark :: Int
         , body :: self } -> Program_ self         -- ^ reset(N, prog)

  Sht :: { mark :: Int
         , exit :: Name
         , body :: self } -> Program_ self         -- ^ shift(N, (k) => prog)

  Ctr :: { iD    :: CtorName
         , arity :: Int} -> Program_ self          -- ^ True, Cons x y

  If  :: { iD      :: CtorName
         , subject :: self
         , yes     :: ([Name], self)
         , no      :: self } -> Program_ self      -- ^ if Cons ls ((x, y) => x) 0
  deriving stock Functor

-- | Wrapper for constructor name.
--
newtype CtorName = CtorName { unCtorName :: Text } deriving stock (Eq)

-- | Program tree reducer.
--
fixpoint :: Functor f => (i -> f a -> a) -> Cofree f i -> a
fixpoint f = go
  where
    go (i :< layer) = f i $ fmap go layer

-- | Program_.toString()
--
instance Pretty1 Program where
  pp1 = fixpoint \_ -> \case
    Var n             -> pp n
    App f xs          -> sexpr f xs
    Lam n p           -> sexpr ("fun" <+> fsep (map pp n) <+> "->") [p]
    Let d b           -> sexpr "let"    (map pp' d ++ ["in", b])
    PC  c             -> pp c
    Rst p b           -> sexpr ("reset" <+> pp p) [b]
    Sht p k b         -> sexpr ("shift" <+> pp p <+> pp k <+> "->") [b]
    Ctr c n           -> sexpr (pp c) (replicate n "_")
    If  c s (ns, y) n -> sexpr "if"     [pp c, s, sexpr "then" (map pp ns ++ ["->", y]), n]
    where
      pp' (n, p) = sexpr (pp n <+> "=") [p]

instance Pretty CtorName where
  pp (CtorName n) = "%" <.> pp n

instance CanBeParsed (Program Range) where
  parser = ranged do
    select
      [ pc
      , ctr
      , var
      , block
      ]
    where
      block = do
        token "("
          *> select
            [ lam
            , let_
            , rst
            , sht
            , if_
            , app
            ]
          <* token ")"

      app = do
        f <- parser
        x <- many parser
        return $ App f x

      var = do
        s <- name
        return $ Var s

      name = do
        s <- match $ does $ (`notElem` Text.words "if let in shift reset fun val -> ( ) / %")
        return $ Name s

      pc = do
        c <- parser
        return $ PC c

      ctr = do
        token "%"
        s <- ctorName
        token "/"
        Num n <- number
        return $ Ctr s (round n)

      ctorName = do
        s <- match $ does $ (`elem` ['A'.. 'Z']) . Text.head
        return $ CtorName s

      if_ = do
        token "if"
        s    <- ctorName
        subj <- parser
        then_ <- do
          token "("
          token "then"
          names <- many name
          token "->"
          then_ <- parser
          token ")"
          return (names, then_)
        else_ <- do
          token "("
          token "else"
          token "->"
          p <- parser
          token ")"
          return p
        return $ If s subj then_ else_

      sht = do
        token "shift"
        Num n <- number
        k     <- name
        token "->"
        p     <- parser
        return $ Sht (round n) k p

      rst = do
        token "reset"
        Num n <- number
        p     <- parser
        return $ Rst (round n) p

      let_ = do
        token "let"
        decls <- many do
          do
            n <- try do
              token "("
              n <- name
              token "="
              return n
            p <- parser
            token ")"
            return (n, p)
        p <- parser
        return $ Let decls p

      lam = do
        token "fun"
        n <- some name
        token "->"
        p <- parser
        return $ Lam n p