
module Name where

import Data.String (IsString (..))
import Data.Text (Text, pack)

import Pretty

newtype Name = Name { unName :: Text }
  deriving Show via PP Name
  deriving stock (Eq)

instance Pretty Name where
  pp = pp . unName