
import Parsing
import Runtime
import Range
import Program
import Pretty

import System.Environment

main = do
  args <- getArgs
  case args of
    [file] -> do
      prog <- parse' @(Program Range) file
      val  <- eval prog
      print val
    _ -> do
      print "USAGE: chilsel <file>"