
# What

A core language for Chisel project.

# Language

The language is call-by-need (lazy), and implemented as CESK machine.

## Constructs

### Comments

A character `;`, followed by anything until a newline.

### Variables

Any sequence of non-space characters, not including `"<()/%;"`, that aren't reserved words: `"if let in shift reset fun val -> ( ) / %"`.

### Constants

There are 3 types of constant: numerics, strings and built-in functions.

Numeric constant has the same definition as haskell double-precision float-point.

String constant is anything between `<>`. No escaping is currently implemented.

Builtins are `#name/arity`. Only `#+/2` and `#less/2` are implemented.

Examples:
```lisp
1
2
<hello, world>
#+/2
#less/2
```

### "Constructors"

The term is quoted, because it just wraps its arguments until later.

The syntax is `%Name/arity`.

Examples:
```lisp
%Pair/2
%True/0
```

### Lambda-functions

Statically-scoped. Can have 1 or more arguments. All functions (including built-ins and constructors) are auto-curried.

Examples:
```lisp
(fun x -> x)
(fun x y -> x)
(fun x y -> (#plus/2 x y))
```

### Application

A call of a function-like object with some (or none) arguments.

Examples:
```lisp
(fold 0 plus list)
((map f) list)
(#plus/2 a 42)
(%Pair/2 <hello> 43)
```

### Let-expression

A construct to declare stuff. Each definition can use each other defunitions in mutually-recursive way, even if they are just values.

This example evaluates to a lazy infinite list of intercalated 0 and 1:
```lisp
(let
  (Cons = %Cons/2)
  (a    = (Cons 0 b))
  (b    = (Cons 1 a))
  a
)
```

### Pattern-match

The construct that allows to use results of applying a constructor. If the value was created with the given constructor, it gives out the arguments to `then` branch. Otherwise it goes into `else` branch.

Example will evaluate to `1`:
```lisp
(let
  (Cons = %Cons/2)
  (Nil  = %Nil/0)
  (list = (Cons 1 Nil))
  (if Cons list (then x xs -> x) (else -> 0))
)
```

### Reset operator

Puts given delimiter in stack. Has no-op semantics by itself.

Example will evaluate to `<hello>`:
```lisp
(prompt 42 <hello>)
```

### Shift operator

Given a prompt, will scan stack until it is found; then it will assign given name to the traversed part of the stack and will expose it as a function into a code block. The traversed part of the stack is removed.

Example will evaluate to `2`:
```lisp
(let
  (plus = #plus/2)
  (reset 42 (plus 1 (shift 42 k -> (k (k 0)))))
)
```

The captured part of the stack is `(plus 1 ...)`, which is called `k` and is applied to `0` twice.
