
Cons = Struct.new :x, :xs
class Nil; end

def replicate x, n
  res = Nil.new
  n.times do
    res = Cons.new x, res
  end
  res
end

def fold zero, op, list
  i = list
  while i.is_a? Cons
    zero = op.(zero, i.x)
    i = i.xs
  end
  zero
end

def sum list
  fold 0, ->(x, y) { x + y }, list
end

puts sum replicate 5, 200000