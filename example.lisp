(let
  ; wrappers for built-in functions
  (plus    = #+/2)
  (less    = #less/2)
  (is-zero = (fun n -> (less n 1)))

  ; best function evar
  (compose = (fun f g x -> (f (g x))))

  ; list constructors
  (Cons = %Cons/2)
  (Nil  = %Nil/0)

  ; list length
  (len = (fold 0 (fun x -> (plus 1))))
  (sum = (fold 0 plus))

  ; list reduce
  (fold = (fun zero op ->
    (let
      (aux = (fun list ->
        (if Cons list
          (then x xs -> (op x (aux xs)))
          (else      -> zero)
        )
      ))
      aux
    )
  ))

  (map = (compose (fold Nil) (compose Cons)))

  ; generate a list of n `x`s
  (replicate = (fun x n ->
    (if True (is-zero n)
      (then -> Nil)
      (else -> (Cons x (replicate x (plus -1 n))))
    )
  ))

  ; sum a list of 20 fives
  (compose sum (map (plus 1)) (replicate 5 200000))
)
