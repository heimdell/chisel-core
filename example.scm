
(define (Cons x xs) (list 'Cons x xs))
(define (Nil)       (list 'Nil))

(define (replicate x n)
  (if (= 0 n)
    (Nil)
    (Cons x (replicate x (- n 1)))))

(define (fold zero op)
  (define (aux lst)
    (if (equal? 'Cons (car lst))
      (begin
        (let
          ((x  (cadr  lst))
           (xs (caddr lst)))
          (op x (aux xs))
        )
      )
      zero))
  aux)

(define sum (fold 0 +))

(print (sum (replicate 5 1000000)))
